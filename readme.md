# Final Year Project 2017

This repository is the backend for a web application that displays the SMS messages and contacts on your phone in real time. Technologies used: Java, Spring Boot, Javascript, Angular 4, Android.

![alt text](sms_home.jpg)

![alt text](smsImageM.jpg)