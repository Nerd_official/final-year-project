package sync.sync.model;
/**
 * Message model class
 *
 * @author Wayne Walker
 */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.SerializableString;
@Entity(name="messages")
@JsonIgnoreProperties(ignoreUnknown=true)
public class Message implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String received;
	private String sent;
	//@Temporal(TemporalType.TIMESTAMP)
	private String date;
	private String number;
	private String name;
	private String uid;
	
	public String getUid() {
		return this.uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int message_no;
	
	public int getId() {
		return id;
	}

	public String getReceived() {
		return received;
	}

	public String getSent() {
		return sent;
	}

	public String getDate() {
		return date;
	}

	public String getNumber() {
		return number;
	}

	public Message() {
		//message_no=1;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setReceived(String received) {
		this.received = received;
	}

	public void setSent(String sent) {
		this.sent = sent;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Message(String message, String sent, String date, String number) {
		this.received = message;
		this.sent = sent;
		this.date = date;
		this.number = number;
		//message_no++;
	}

	public static int getMessage_no() {
		return message_no;
	}

	public static void setMessage_no(int message_no) {
		Message.message_no = message_no;
	}

	
	@Override
	public String toString() {
		return "Message [id=" + id + ", received=" + received + ", sent=" + sent + ", date=" + date + ", number="
				+ number + "]";
	}

	public String getName() {
		return name;
	}	
	
}
