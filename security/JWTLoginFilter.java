package sync.sync.security;
/**
 * JWTLogin filter class
 *
 * Taken from https://auth0.com/blog/securing-spring-boot-with-jwts/
 */
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.tasks.OnFailureListener;
import com.google.firebase.tasks.OnSuccessListener;
import com.google.firebase.tasks.Task;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {
	private int count;

	  public JWTLoginFilter(String url, AuthenticationManager authManager) {
	    super(new AntPathRequestMatcher(url));
	    setAuthenticationManager(authManager);
	    setAuthenticationSuccessHandler(new JwtSuccessHandler());
	   
	  }
	
	  @Override
	  public Authentication attemptAuthentication(
	      HttpServletRequest req, HttpServletResponse res)
	      throws AuthenticationException, IOException, ServletException {
		  String header = req.getHeader("Authorization-X");
		  Task<FirebaseToken> authTask;
		  
		  if(header != null || header !=""){ 
          	authTask = FirebaseAuth.getInstance().verifyIdToken(header)
         		    .addOnSuccessListener(new OnSuccessListener<FirebaseToken>() {
         		        @Override
         		        public void onSuccess(FirebaseToken decodedToken) {
         		            String uid = decodedToken.getUid();
         		            String user = decodedToken.getEmail();
         		        } 
         		       })
         		      .addOnFailureListener(new OnFailureListener() {				
         		      	@Override
         		      	public void onFailure(Exception arg0) {
         		      	    throw new RuntimeException("Firebase login failed"); 
         		      	 
         		      	}
         	      	    });
          	 count++
         		return getAuthenticationManager().authenticate(
         		        new UsernamePasswordAuthenticationToken(
         		            "admin",
         		            "password",
         		            Collections.emptyList()
         		        )
         		    );
		}
		  return null;
	    
	  }

	  @Override
	    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
	        super.successfulAuthentication(request, response, chain, authResult);
	        System.out.println("success auth called");
	        TokenAuthenticationService
	        .addAuthentication(response, "admin");
	        chain.doFilter(request, response);
	    }
}
