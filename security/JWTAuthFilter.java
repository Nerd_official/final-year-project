package sync.sync.security;
/**
 * Auth filter class
 *
 * Taken from https://auth0.com/blog/securing-spring-boot-with-jwts/
 */
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import org.springframework.security.core.Authentication;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class JWTAuthFilter extends GenericFilterBean {

	  @Override
	  public void doFilter(ServletRequest request,
	             ServletResponse response,
	             FilterChain filterChain)
	      throws IOException, ServletException {
		  
		  HttpServletRequest httpRequest = (HttpServletRequest)request;
		  
	      String header = httpRequest.getHeader("Authorization-Bearer");
	      System.out.println("auth header"+ header);
	       if (header != null  && !header.startsWith("Bearer")) {
	    		  throw new RuntimeException("JWT Auth Token is not good");  
		   } 
	 
	       System.out.println("AuthFilter called" );
	       Authentication authentication = TokenAuthenticationService
			        .getAuthentication(httpRequest);
			
		   SecurityContextHolder.getContext()
			        .setAuthentication(authentication);
		   filterChain.doFilter(request,response);
	                 
	  }	  
}
