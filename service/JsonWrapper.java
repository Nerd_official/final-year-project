package sync.sync.service;

import java.util.List;

import sync.sync.model.Message;

public class JsonWrapper {
	List<Message> newMessage;

	public List<Message> getNewMessage() {
		return newMessage;
	}

	public void setNewMessage(List<Message> newMessage) {
		this.newMessage = newMessage;
	}

}
