package sync.sync.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;

import sync.sync.model.Message;

public interface MessageImpl {

	 public CompletableFuture findUser(HttpServletRequest req, Message message)throws InterruptedException;
}
