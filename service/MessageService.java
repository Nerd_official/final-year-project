package sync.sync.service;
/**
 * Message Service class communicates with Message Repository class
 * Returns messages to the Rest Controller
 *
 * @Author Wayne Walker 
 */
import java.lang.reflect.Field;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.google.firebase.auth.FirebaseAuth;
import sync.sync.dao.ContactRepo;
import sync.sync.dao.MessageRepo;
import sync.sync.model.Contact;
import sync.sync.model.Message;
@Service
@Transactional
public class MessageService {
static int count = 0;
private final MessageRepo messageRepo;
private FirebaseAuth mAuth;

@Autowired
private ContactService contactService;	

	public MessageService(MessageRepo messageRepo) {
		this.messageRepo = messageRepo;		
	}
	
	public void save(Message message) {
		count++;
		messageRepo.save(message);
		
	}
	public static String getUserName() {
        return "Real user name";
    }
	//Find all messages
	public List<Message> findAll() {
	
		List<Message> messages = new ArrayList<>();
		for(Message m : messageRepo.findAll()) {
			messages.add(m);
			
		}
		return messages;
	}
	//Get requested conversation
	public List<Message> findMessage2(String uid, String number, int page) {
		List<Message> messages = new ArrayList<>();
			for(Message m : messageRepo.findByUidAndNumber(uid, number, new PageRequest(page,10,Direction.DESC,"date"))) {
				//Find name
				String findName = contactService.matchNumber(m.getNumber(),1, uid);
				//If name does not exist set number
				if(findName.equals(""))
				     m.setName(number);
				else
					 m.setName(findName);
				
				messages.add(m);	
			}
		return messages;
	}
	
	 //Get one item from database
    public Message getTopic(int id) {
    	return messageRepo.findOne(id);
    }
    //Check if date exists, if it does not then insert values into database
    public int findNumber(String date) throws SQLIntegrityConstraintViolationException {
    	List<Object> messages = new ArrayList<>();
    	try {
			for(Object  m : messageRepo.findByDate(date)) {
				messages.add(m);
			}
    	} 
		catch(Exception e)
		{
		 System.out.println("Execption with primary key");
		}
		return messages.size();
    }
    
    //Return the top received messages
    public List<Message> findMessages(String uuid) {
    	List<Message> messages = new ArrayList<>();
		//Taken from: https://stackoverflow.com/questions/20486641/ljava-lang-object-cannot-be-cast-to
		List<Object> result = (List<Object>) messageRepo.findReceivedByDate(uuid); 
		Iterator itr = result.iterator();
		while(itr.hasNext()){
		   Object[] obj = (Object[]) itr.next();
		   //now you have one array of Object for each row
		   String sms = String.valueOf(obj[0]);
		   String sent = String.valueOf(obj[1]);
		   String date = String.valueOf(obj[2]); 
		   String number = String.valueOf(obj[3]);
		   //Get name from contact table 
		   String findName = contactService.matchNumber(number,1, uuid);
		  
		   Message addName = new Message(sms,sent,date,number);
		   addName.setName(findName);
		   messages.add(addName);	   
		}	
		return messages;
	}
	    

}
