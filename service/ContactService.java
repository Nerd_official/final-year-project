package sync.sync.service;
/**
 * Contact Service class communicates with the contacts repository class
 * Returns contacts to the Rest Controller 
 *
 * @Author Wayne Walker 
 */
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import sync.sync.dao.ContactRepo;
import sync.sync.model.Contact;


@Service
@Transactional
public class ContactService {
	private final ContactRepo contactRepo;
    //Constructor
	public ContactService(ContactRepo contactRepo) {
		this.contactRepo = contactRepo;
	}
	//Finds a contact by its number
	public String matchNumber(String number,int i, String uuid) {
		//Match database number by adding country code to number
		if (number.length() > 8)
		if (number.substring(0, 2).equals("00")) {
	        number = "+" + number.substring(2);
	    } else if (number.substring(0, 1).equals("0")) {
	        number = "+44" + number.substring(1);
	    }
		String found ="";
		for(Contact task : contactRepo.findByNumberAndUuid(number, uuid)) {
			found = task.getName();
		}
		
		return found;
	}
	//Get all contacts
	public List<Contact> getContactData(String uuid) {
		List<Contact> data = new ArrayList<>();
		for(Contact contact : contactRepo.findByUuidOrderByNameAsc(uuid)) {
			data.add(contact);
		}
		return data;
	}
}
