package sync.sync.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import sync.sync.dao.MessageRepo;
import sync.sync.model.Data;
import sync.sync.model.Message;

@Service
public class DataService{
	@Autowired
	private MessageService messageService;
    public DataService(){
    	
    }
	Data mData = new Data();
	String date = "01/01/2000";
	static List<Object> message = new ArrayList<Object>();
	static List<Object> test = new ArrayList<Object>();
	static List data = new ArrayList();
	static String hello ="";
	String display = "<ul>";
	static int count = 0;
	public void newData(HttpServletRequest req) {
		
		List newList = new ArrayList<String>();
		
		DatabaseReference ref = FirebaseDatabase
	    	    .getInstance()
	    	    .getReference("74");
		ref.orderByChild("mum").limitToLast(4);
	    ref.addValueEventListener(new ValueEventListener() {
	    	    @Autowired
	    	    @Override
	    	    public void onDataChange(DataSnapshot dataSnapshot) {
	    	    	Message newPost = dataSnapshot.getValue(Message.class);
	    	    	DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
					 Map<String, Object> value = new HashMap<String, Object>();
					 value.put("date", newPost.getDate());
	    	    	Object document = dataSnapshot.getValue();	
	    	    }	
				@Override
				public void onCancelled(DatabaseError arg0) {
					// TODO Auto-generated method stub					
				}
	    	});
	}
	
	public void childData (HttpServletRequest req){
		final DatabaseReference ref = FirebaseDatabase.getInstance().getReference("74");
		ref.orderByChild("mum").addChildEventListener(new ChildEventListener() {
			
			@Override
			public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
				 Message b = new Message("message", " me", "date", "number");
				Date newd = new Date();
				data.clear();
				for (DataSnapshot child : snapshot.getChildren()) {
					count++;
					 Map<String, Object> value = (Map<String, Object>)child.getValue();
					 String received = String.valueOf(value.get("received"));
					 String sent = String.valueOf(value.get("sent"));
					 String date = String.valueOf(value.get("date"));
					 String number = String.valueOf(value.get("number"));
					 Message c = new Message(received, sent, date, number);
								try {
									findUser(req,b);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
					messageService.save(c);
					data.add(value);
					date = String.valueOf(value.get("date"));
					req.setAttribute("messageTasks",data);
					req.setAttribute("test", "test");	
				}
			}

			@Override
			public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
				// TODO Auto-generated method stub
				 System.out.println("test test changed");
							
			}

			@Override
			public void onChildRemoved(DataSnapshot snapshot) {
				// TODO Auto-generated method stub
				System.out.println("test test removed");
			}

			@Override
			public void onChildMoved(DataSnapshot snapshot, String previousChildName) {
				// TODO Auto-generated method stub
				System.out.println("test test moved");
			}

			@Override
			public void onCancelled(DatabaseError error) {
				// TODO Auto-generated method stub
				
			}
			 
	    });
	}
	
	     @Async
	     public CompletableFuture findUser(HttpServletRequest req, Message message) throws InterruptedException {

	         
	    	 try {
	    			Thread.sleep(1000L);
	    		//	messageService.save(message);
	   	    	 System.out.println("Looking up " +  message);
	    		} catch (InterruptedException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	         
	    	 
	         return CompletableFuture.completedFuture(data);
	 
	     }

}

	