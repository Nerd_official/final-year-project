package sync.sync.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

@Configuration
public class FirebaseConfig {
	FirebaseOptions options;
	
	public FirebaseConfig() {
		
	}
	@PostConstruct
	public void test() throws IOException{
		//Json provided by Firebase 
		String path = "C:/Users/xxxx/Documents/workspace-sts-3.9.1.RELEASE/sync/xxxxxx.json/";
		FileInputStream serviceAccount = null;
		try {
			serviceAccount = new FileInputStream(path);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
        options = new FirebaseOptions.Builder()
        		.setCredential(FirebaseCredentials.fromCertificate(serviceAccount))
                .setDatabaseUrl("https://xxxxxx.firebaseio.com/").build();
        FirebaseApp.initializeApp(options);   	
	}
}
