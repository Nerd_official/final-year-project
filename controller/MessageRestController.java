package sync.sync.controller;
/**
 * RestController class
 *
 * @author Wayne Walker
 */
import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sync.sync.model.Contact;
import sync.sync.model.Message;
import sync.sync.service.ContactService;
import sync.sync.service.MessageService;

@RestController
public class MessageRestController {
	
		@Autowired
		private ContactService contactService;
		
		@Autowired
		private MessageService messageService;
		Message message = new Message();
		
		@GetMapping("/login1")
		public String newLogin(HttpServletRequest request){
			return "login";
		}
		
		@GetMapping(value = "/json", produces = MediaType.APPLICATION_JSON_VALUE)
		public Message newJson(HttpServletRequest request){
			Message message = new Message();
			message.setName("David");
			message.setReceived("hello world");
			return message;
		}	
		
		//Get the latest phone messages
		@GetMapping("received")
		public @ResponseBody List<Message> getMessage(@RequestHeader(value="Authorization-Bearer", required=false) String userAgent, 
				@RequestHeader(value="uuid", required=false) String id) {
			return messageService.findMessages(id);
		}
		
		//Save messages from Firebase
		@PostMapping(value="/received/messages")
		public Message saveMessage(@RequestHeader(value="Authorization-Bearer", required=false)String userAgent, @RequestBody Message message, BindingResult bindingResult, HttpServletRequest req) throws IOException, SQLIntegrityConstraintViolationException {

			 //Check if date exists in database
			 int test= messageService.findNumber(message.getDate());
			 if(test == 0){
				 messageService.save(message);
				 System.out.println("Safe to insert "+ message.getDate());
			 } 
			return message;
		}
		
		//Pagination
		@GetMapping("/received/{uid}/{id}/page/{page}")
		public List<Message> allMessage(@RequestHeader(value="Authorization-Bearer", required=false) String userAgent, 
				@RequestHeader(value="uuid", required=false) String uuid, @PathVariable String uid, @PathVariable String id, @PathVariable int page, HttpServletRequest req) {
			for (Message me: messageService.findMessage2(uid,id,page)) {
				    System.out.println("page me "+me);
			
			}
			return messageService.findMessage2(uid,id,page);
		}
		//Verify Firebase Token
		@GetMapping("/received/verifyId")
		public List<String> verifyId(HttpServletResponse res, @RequestHeader(value="Authorization-X", required=false) String userAgent) {
			List<String> jwtString = new ArrayList<>();
		return jwtString;
		}
		//Get contact data
		@GetMapping("/received/contacts")
		public @ResponseBody List<Contact> getContacts(@RequestHeader(value="Authorization-Bearer", required=false) String userAgent, 
				@RequestHeader(value="uuid", required=false) String id) {
			return contactService.getContactData(id);
		}
		
	
}
