package sync.sync.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
		
		@RequestMapping("/")
		public String index(HttpServletRequest req) throws Exception {
				return "index";
		}

		@GetMapping("/login")
		public String newLogin(HttpServletRequest request){
			return "login";
		}		
}
