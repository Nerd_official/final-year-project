package sync.sync.dao;

import org.springframework.data.repository.CrudRepository;

import sync.sync.model.User;

public interface UserRepo extends CrudRepository<User, Long> {
	 
    User findByUsername(String username);
}
