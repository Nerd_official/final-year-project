package sync.sync.dao;
/**
 * Message Repository class that get messages from database
 *
 * @author Wayne Walker
 */

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import sync.sync.model.Message;


public interface MessageRepo extends PagingAndSortingRepository<Message, Integer>{
     //Find message by uid, number and return the pageable amount
     Page<Message>findByUidAndNumber(String uid, String number, Pageable pageable);
     
     // Find a message by date
	@Query("Select received, date, number from messages a where a.date like %?1%")
	List<Object> findByDate(String date);
	
	//get all received messages by date
	@Query("SELECT a.received, a.sent, a.date, a.number FROM messages a WHERE a.date in( SELECT MAX(date) FROM messages a WHERE a.uid like %?1% GROUP by a.number) ORDER BY date DESC")
	List<Object> findReceivedByDate(String uuid);
}
