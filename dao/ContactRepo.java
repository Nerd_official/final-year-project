package sync.sync.dao;
/**
 * Contacts Repository class gets data from the database
 *
 * @author Wayne Walker
 */
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import sync.sync.model.Contact;


public interface ContactRepo extends CrudRepository<Contact, Integer>{
    //Find a contact by number
	List<Contact> findByNumber(String number);
	//Find a contact by number and uuid
	List<Contact> findByNumberAndUuid(String number, String uuid);
	//Find a contact by uuid 
	List<Contact> findByUuidOrderByNameAsc(String uuid);
	
}
